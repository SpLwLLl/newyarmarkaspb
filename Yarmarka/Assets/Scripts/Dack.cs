using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dack : MonoBehaviour
{
    public int health = 1;

    public void TakeDamage(int damage)
    {
        if (health > damage)
        {
            health -= damage;
        }
        else
        {
            ScoreDack.Instance.PlusBanka();
            Destroy(gameObject);
        }
    }
}
