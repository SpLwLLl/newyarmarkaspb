using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Gun : MonoBehaviour
{
    public AudioClip fireClip;
    public AudioSource audioSource;
    private Interactable interactable;
    public SteamVR_Action_Boolean fireAction;
    public Transform Dulo;
    public int Damage = 1;
    public GameObject Bullet;
    public float speed = 10f;
    
    // Start is called before the first frame update
    private void Start()
    {
    
        interactable = GetComponent<Interactable>();
    }

    // Update is called once per frame
    public void Update()
    {

        Ray ray = new Ray(Dulo.position, Dulo.forward);
        Debug.DrawRay(Dulo.position, Dulo.forward * 100, Color.yellow);


        if (interactable.attachedToHand != null)
        {
            SteamVR_Input_Sources hand = interactable.attachedToHand.handType;
            if(fireAction[hand].stateDown)
            {
                Fire();
            }
        }
    }
    public void Fire()
    {

       
        RaycastHit hit;
        if (Physics.Raycast(Dulo.position, Dulo.forward, out hit, 300))
        {
            if(hit.transform.GetComponent<mishen>())
            {
                hit.transform.GetComponent<mishen>().TakeDamage(Damage);
            }
            if (hit.transform.GetComponent<Dack>())
            {
                hit.transform.GetComponent<Dack>().TakeDamage(Damage);
            }
        }
        Rigidbody BulletRigidbody = Instantiate(Bullet, Dulo.position, Dulo.rotation).GetComponent<Rigidbody>();
        BulletRigidbody.velocity = Dulo.up;
    }
}
