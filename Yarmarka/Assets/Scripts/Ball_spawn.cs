using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_spawn : MonoBehaviour
{
    public Transform pointRespawn;
    public GameObject Ball;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
             Instantiate(Ball, pointRespawn.position, Quaternion.identity);
            Destroy(other);
        }
    }
}
