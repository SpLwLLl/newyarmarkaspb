using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ScoreDisplay : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public float banka;
    public float Mesh;
    public static ScoreDisplay Instance { get; set; }
    //public static Action bankaPush;
    public GameObject NextLevel;
    private void Start()
    {
      //  bankaPush += PlusBanka;
    }
    private void Awake()
    {
        Instance = this;
    }
    public void PlusBanka()
    {
        banka += 1;
        scoreText.text = ((int)banka).ToString();
    }
    
    public void win()
    {
        if(banka >= 10)
        {
            (NextLevel).SetActive(false);
        }
    }
}