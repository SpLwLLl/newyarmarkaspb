﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class controlertotem : MonoBehaviour
{
    public float tottem;
    public static controlertotem Instance { get; set; }
    public GameObject Teleport;
    private void Awake()
    {
        Instance = this;
    }

    public void DestroyTotem()
    {
        tottem += 1;
    }

    void Update()
    {
        if (tottem >= 3)
        {
            Teleport.SetActive(true);
        }
    }
    void Start()
    {
        Teleport.SetActive(false);
    }

    //controlertotem.Instance.DestroyTotem();
}



