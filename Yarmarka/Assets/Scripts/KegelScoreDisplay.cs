using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;


public class KegelScoreDisplay : MonoBehaviour
{
    public float Kegla;
    public static KegelScoreDisplay Instance { get; set; }
    public static Action keglaPush;
    public TextMeshProUGUI KeglaText;
    public GameObject NextLevel;
    private void Start()
    {
        keglaPush += PlusKegla;
    }
    private void Awake()
    {
        Instance = this;
    }
    public void PlusKegla()
    {
        Kegla += 1;
        KeglaText.text = ((int)Kegla).ToString();
    }
    public void win()
    {
        if (Kegla >= 10)
        {
            (NextLevel).SetActive(false);
        }
    }
}
