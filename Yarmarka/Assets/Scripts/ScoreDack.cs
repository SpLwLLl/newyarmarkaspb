using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreDack : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public float banka;
    public static ScoreDack Instance { get; set; }
    //public static Action bankaPush;
    
    private void Start()
    {
        //  bankaPush += PlusBanka;
    }
    private void Awake()
    {
        Instance = this;
    }
    public void PlusBanka()
    {
        banka += 1;
        scoreText.text = ((int)banka).ToString();
    }
}
