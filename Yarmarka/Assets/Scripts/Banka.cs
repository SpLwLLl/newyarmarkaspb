using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Banka : MonoBehaviour
{ 
    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Ball"))
        {
          //  ScoreDisplay.bankaPush?.Invoke();
            ScoreDisplay.Instance.PlusBanka();
            Destroy(gameObject);
        }
       
    }
}
