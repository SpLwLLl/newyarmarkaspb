using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kegel : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Kegla"))
        {
            KegelScoreDisplay.Instance.PlusKegla();
            Destroy(gameObject);
        }

    }
}
