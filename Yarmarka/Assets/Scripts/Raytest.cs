using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raytest : MonoBehaviour
{
    public Transform Dulo1;
    public int Damage1 = 10;

    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(transform.position, transform.forward * 100, Color.yellow);

        RaycastHit hit;
        if (Physics.Raycast(Dulo1.position, Dulo1.forward, out hit, 300))
        {
            if (hit.transform.GetComponent<mishen>())
            {
                hit.transform.GetComponent<mishen>().TakeDamage(Damage1);
            }
            if(hit.transform.GetComponent<Dack>())
            {
                hit.transform.GetComponent<Dack>().TakeDamage(Damage1);
            }
        }
    }
}
