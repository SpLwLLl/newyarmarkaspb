using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreBall : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public float banka;
    public static ScoreBall Instance { get; set; }
    //public static Action bankaPush;

    private void Start()
    {
        //  bankaPush += PlusBanka;
    }
    private void Awake()
    {
        Instance = this;
    }
    public void PlusBanka()
    {
        banka += 1;
        scoreText.text = ((int)banka).ToString();
    }
}
