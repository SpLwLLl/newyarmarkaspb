using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BawlingBall : MonoBehaviour
{
    public Transform BowlingPointRespawn;
    public GameObject BowlingBall;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BowlingBall"))
        {
             Instantiate(BowlingBall, BowlingPointRespawn.position, Quaternion.identity);
            Destroy(other);
        }
    }
}
