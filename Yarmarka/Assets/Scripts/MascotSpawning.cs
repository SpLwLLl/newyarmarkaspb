using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MascotSpawning : MonoBehaviour
{
    public GameObject Mascot;
    // Start is called before the first frame update


    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Mascot.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Mascot.SetActive(false);

        }
    }
}
