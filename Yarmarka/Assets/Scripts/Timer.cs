using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerOnTouch : MonoBehaviour
{
    public Text timerText;
    public float duration = 60f;
    private float elapsedTime;
    public GameObject StartButton;
    public GameObject End;

    private void Start()
    {
        enabled = false;
        elapsedTime = 0f;
        End.SetActive(false);
        StartButton.SetActive(true);
    }

    private void Update()
    {
        elapsedTime += Time.deltaTime;
        print(elapsedTime +" : "+ duration);
        if (elapsedTime >= duration)
        {
            elapsedTime = duration;
            StopTimer();
        }
        UpdateTimerText();
    }

    private void StopTimer()
    {
        End.SetActive(true);
        enabled = false;
        
    }

    private void UpdateTimerText()
    {
        if (timerText != null)
        {
            float remainingTime = duration - elapsedTime;
            float minutes = Mathf.FloorToInt(remainingTime / 60);
            float seconds = Mathf.FloorToInt(remainingTime % 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartTimer();
            StartButton.SetActive(false);
            
        }
    }

    private void StartTimer()
    {
        elapsedTime = 0f;
        enabled = true;
    }
}